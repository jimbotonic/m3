### Initialize Database
```
cd {PROJET_FOLDER}
source .env/bin/activate
python app/init_db.py
```

### Query Database
```
cd {PROJET_FOLDER}
source .env/bin/activate
python
>>> import app.db as db
>>> db = db.db_handler()
>>> db.query("SELECT * FROM users")
```

### Generate Password Hash
```
cd {PROJET_FOLDER}
source .env/bin/activate
python
>>> from app.utils import hash_sha1
>>> hash_sha1("1234")
'7110eda4d09e062aa5e4a390b0a572ac0d2c0220'
```
