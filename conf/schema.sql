CREATE TABLE user
(
	id INTEGER PRIMARY KEY,
	username VARCHAR(64) NOT NULL UNIQUE,
	passwd_hash VARCHAR(64) NOT NULL,
	first_name VARCHAR(64) DEFAULT NULL,
	last_name VARCHAR(64) DEFAULT NULL,
	email VARCHAR(128) DEFAULT NULL
);

CREATE UNIQUE INDEX username_idx ON user (username);

INSERT INTO user (username, passwd_hash, first_name, last_name, email) VALUES ('test','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','first','last','test@edu.ge.ch');


